<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
//use LaravelBook\Ardent\Ardent;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Protection against mass assignment
	 *
	 */
	protected $fillable = array('firstname', 'lastname', 'email');
	protected $guarded = array('user_id', 'password');

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getPassword()
	{
	  return $this->password;
	}
	
	/**
	 * One-One relation with Topic(5 topics he wants to find on the site)
	 *
	 */
	public function topic()
    {
        return $this->hasOne('Topic', 'user_id');
    }

    /**
     * Many-Many relation with Interest(his interests)
     *
     */
    public function interest()
    {
    	return $this->hasMany('Interest', 'user_id');
    }
}	
