<?php

class Topic extends \Eloquent {
	protected $fillable = array('t1', 't2', 't3', 't4', 't5');

	public function user()
    {
        return $this->belongsTo('User');
    }
}