<?php

class Interest extends \Eloquent {
	protected $fillable = array('topic');

	public function user()
    {
    	return $this->belongsTo('User');
    }
}