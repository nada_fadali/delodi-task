<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>What is it?</title>

		<link rel="stylesheet" type="text/css" href="main.css">
	</head>
	<body>
		<h1>A very super cool website</h1>
		<div class="container">
			{{ Form::open(array('route' => 'user.home', 'class' => 'inline')) }}
			{{ Form::label('email', 'Take Survey?') }}&emsp;
			{{ Form::submit('Maybe Later', array('name' => 'decline', 'class' => 'button')) }}
			{{ Form::close() }}

			{{ Form::open(array('route' => 'survey.show', 'class' => 'inline')) }}
			{{ Form::submit('Yes', array('name' => 'accept', 'class' => 'button')) }}
			{{ Form::close() }}

		</div>
	</body>
</html>

