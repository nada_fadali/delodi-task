<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Survey:side 1</title>

	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<h1>A very super cool website</h1>
	<div class="container" style="text-align:left">


		<span>All fields are required. Proceed with caution.</span>;

		{{ Form::open(array('route' => 'survey.create')) }}

		<ol>
			<li> {{ Form::label('q1', 'Which type of occupation discribes you best?') }} </li> 
			{{ Form::radio('a1', 'pupil') }} pupil<br>
			{{ Form::radio('a1', 'student') }} student<br>
			{{ Form::radio('a1', 'employee') }} employee<br>
			{{ Form::radio('a1', 'civil servant') }} civil servant<br>
			{{ Form::radio('a1', 'self employed') }} self employed<br>
			{{ Form::radio('a1', 'other')}}other: {{ Form::text('a10')}}<br><br>

			<li>{{ Form::label('q2', 'How often do you visit our website?') }} </li>
			{{ Form::radio('a2', '1-3') }} 1-3 times<br>
			{{ Form::radio('a2', '3-5') }} 3-5 times<br>
			{{ Form::radio('a2', '>5') }} more than 5 times<br><br>

			<li>{{ Form::label('q3', "In which of our website's topics are you mostly interested in?(multiple choices)") }} </li>
			{{ Form::radio('a30', 'politics') }} politics<br>
			{{ Form::radio('a31', 'culture') }} culture<br>
			{{ Form::radio('a32', 'sports') }} sports<br>
			{{ Form::radio('a33', 'people') }} people<br>
			{{ Form::radio('a34', 'business') }} business<br><br>
		</ol>


		{{ Form::submit('Next Page', array('class' => 'button'))}}
		{{ Form::close() }}

	</div>
</body>
</html>