<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Survey:side 1</title>

	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<h1>A very super cool website</h1>
	<div class="container" style="text-align:left">

	<span>All fields are required. Proceed with caution.</span>

	{{ Form::open(array('route' => 'survey.two')) }}

	<ol start='4'>
		<li> {{ Form::label('q4', 'Which other topics would you like to find on our website? Please click on each in the order you wish to sort them according to your preferences: -') }} </li>
		<input name="a40" type="checkbox" value="internet" onclick="setChecks(this)"/> music
		<input name="order0" type="hidden" id="lbl0"/> <br>

		<input name="a41" type="checkbox" value="music" onclick="setChecks(this)"/> internet
		<input name="order1" type="hidden" id="lbl1"/> <br>

		<input name="a42" type="checkbox" value="local" onclick="setChecks(this)"/> local
		<input name="order2" type="hidden" id="lbl2"/> <br>

		<input name="a43" type="checkbox" value="travel" onclick="setChecks(this)"/> tarvel
		<input name="order3" type="hidden" id="lbl3"/> <br>

		<input name="a44" type="checkbox" value="cars" onclick="setChecks(this)"/> cars
		<input name="order4" type="hidden" id="lbl4"/> <br>

		<li>{{ Form::label('q5', 'Are there any other people living with You?') }} </li>
		<!--{{ Form::radio('a5', 'yes') }} yes<br>-->
		{{ Form::radio('a5', 'no') }} no<br>
		if yes <br>
			&emsp;{{ Form::radio('a5', 'parent') }} I'm living with my parents<br>
			&emsp;{{ Form::radio('a5', 'flat') }} I'm living in a flat share community<br>
			&emsp;{{ Form::radio('a5', 'partner') }} I'm living with a partner <br>	

		<li>{{ Form::label('q6', 'How old are you?') }} </li>
		{{ Form::radio('a6', '<18') }} below 18<br>
		{{ Form::radio('a6', '18-25') }} 18-25<br>
		{{ Form::radio('a6', '25-32') }} 25-32<br>
		{{ Form::radio('a6', '32-40') }} 32-40<br>
		{{ Form::radio('a6', '41-50') }} 41-50<br>
		{{ Form::radio('a6', '>50') }} over 50<br>
	</ol>


	{{ Form::submit('Done')}}
	{{ Form::close() }}


	<script type="text/javascript">
		var counter = 0;
		function setChecks(obj){
			if (obj.checked) {
				switch(obj.value){
					case 'internet': document.getElementById('lbl0').value = counter;break;
					case 'music': document.getElementById('lbl1').value = counter;break;
					case 'local': document.getElementById('lbl2').value = counter;break;
					case 'travel': document.getElementById('lbl3').value = counter;break;
					case 'cars': document.getElementById('lbl4').value = counter;break;
				}
				counter++;
			}
		}
	</script>
	</div>
</body>
</html>