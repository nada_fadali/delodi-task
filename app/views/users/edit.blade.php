@if($errors->any())
  <ul>
    {{ implode('', $errors->all('<li>:message</li>'))}}
  </ul>
@endif

{{ Form::model($user, array('route' => array('user.update', $user->id))) }}
 
{{ Form::close() }}