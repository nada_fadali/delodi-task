<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Welcome</title>

	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<h1>A very super cool website</h1>
	<div class="container">
		{{ Form::open(array('route' => 'log.val')) }}
 
		  {{ Form::label('email', 'Email') }}
		  {{ Form::text('email', '', array('class' => 'float')) }}<br><br><br>
		 
		  {{ Form::label('password', 'Password') }}
		  {{ Form::password('password', array('class' => 'float')) }}<br><br><br>
		 
		  {{ Form::submit('Submit', array('class' => 'button')) }}
		 
		{{ Form::close() }}

		@if(Session::has('failure'))
		  <span>Username or password incorrect.</span>
		@endif
	</div>
</body>
</html>



