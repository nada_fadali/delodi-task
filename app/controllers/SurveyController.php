<?php

class SurveyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /survey
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /survey/create
	 *
	 * @return Response
	 */
	public function create()
	{
		// check none is empty
		$q1 = (Input::get('a1') == 'other')? Input::get('a10') : Input::get('a1');
		$q2 = Input::get('a2');

		if($q1 == null || $q2 == null 
			||(Input::get('a30') == null && Input::get('a31') == null 
			&& Input::get('a32') == null && Input::get('a33') == null && Input::get('a34') == null)){
			// missing data
			return View::make('survey.page1');
		}
		else
		{
			$q3 = [Input::get('a30'), Input::get('a31'), Input::get('a32'), 
					Input::get('a33'), Input::get('a34')];

			// save session
			Session::put('q1', $q1);
			Session::put('q2', $q2);
			Session::put('q3', $q3);

			// go to side two
			return View::make('survey.page2');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /survey
	 *
	 * @return Response
	 */
	public function store()
	{	
		// check
		//$q5 = (Input::get('a5') == 'yes')? Input::get('a50') : Input::get('a5');
		$q5 = Input::get('a5');
		$q6 = Input::get('a6');

		if ($q5 == null || $q6 == null 
			||(Input::get('a40') == null && Input::get('a41') == null && Input::get('a42') == null
				&& Input::get('a43') == null && Input::get('a44') == null))
		{
			return View::make('survey.page2');
		}
		else
		{	
			$order = array(
				'music' => Input::get('order0'), 
				'internet' => Input::get('order1'), 
				'local' => Input::get('order2'), 
				'travel' => Input::get('order3'), 
				'cars' => Input::get('order4'), 
			);
			asort($order);
			$q4 = array_flip($order);

			// update DB
			$user = DB::table('users')->where('user_id', Session::get('user'));
			$user->update(array('job' => Session::get('q1')));
			$user->update(array('visit_rate' => Session::get('q2')));
			$user->update(array('living' => $q5));
			$user->update(array('age_range' => $q6));

			DB::table('topics')->insert(
				array(
					'user_id' => Session::get('user'),
					't1' => $q4[0],
					't2' => $q4[1],
					't3' => $q4[2],
					't4' => $q4[3],
					't5' => $q4[4]
			));


			$interest = DB::table('interests');
			foreach ( Session::get('q3') as $key => $value) {
				if($value != null)
					$interest->insert(array('user_id' => Session::get('user'), 'topic' => $value));
			}

			// done
			return Redirect::route('user.home');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /survey/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /survey/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /survey/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /survey/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Session::forget('q1');
		Session::forget('q2');
		Session::forget('q3');
	}

}