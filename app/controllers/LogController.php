<?php

class LogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /login
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /login/create
	 *
	 * @return Response
	 */
	public function create()
	{
		// show view for login
		return View::make('log.in');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /login
	 *
	 * @return Response
	 */
	public function store()
	{	
		$email = Input::get('email');
		$password = Input::get('password');

		if (Auth::attempt(array('email' => $email, 'password' => $password)))
	    {
	      return Redirect::intended('/');
	    }
	    
	    	######
	    ####	#####

	    	// validate
	    $user = User::where('email', '=', $email)->first();
	    Session::put('user', $user->user_id);
	    if($user != null) {
		    if($user->getPassword() == $password)
		    {
		    	// check if user took survey
		    	if ($user->survey == 0)
		    	{
		    		return View::make('survey.choice');
		    	}
		    	else
		    	{
		    		return View::make('users.home');
		    	}
		    }
		}
	   
	    	// invalid email & password
	    return Redirect::back()->withInput()->with('failure', 'failed');

	}

	/**
	 * Display the specified resource.
	 * GET /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /login/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		//
		Auth::logout();
 		Session::flush();
    	// render logout
    	return View::make('log.out');
	}

}