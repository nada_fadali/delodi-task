<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
	
Route::get('/', function()
{
	return View::make('log.in');
});

// auth
Route::get('login', array(
  'uses' => 'LogController@create',
  'as' => 'log.in'
));

Route::post('login', array(
  'uses' => 'LogController@store',
  'as' => 'log.val'
));

Route::get('logout', array(
  'uses' => 'LogController@destroy',
  'as' => 'log.out'
));


Route::post('dashboard', array(
	'as' => 'user.home',
	function(){
		return View::make('users.home');
	}
));


Route::any('dashboard', array(
	'as' => 'user.home',
	function(){
		return View::make('users.home');
	}
));


Route::post('survey', array(
	'as' => 'survey.show',
	function(){
		$user = DB::table('users')->where('user_id', Session::get('user'));
		$user->update(array('survey' => 1));
		return View::make('survey.page1');
	}
));

Route::any('survey', array(
	'as' => 'survey.create',
	'uses' => 'SurveyController@create'
));

Route::post('survey/cal', array(
	'as' => 'survey.two',
	'uses' => 'SurveyController@store'
));

