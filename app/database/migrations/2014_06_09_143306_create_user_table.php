<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('user_id');
			//$table->integer('user_id');
			$table->string('firstname', 30)->default('');
			$table->string('lastname', 30)->default('');
			$table->string('email', 80)->unique();
			$table->string('password', 100)->default('');
			$table->string('job', 70)->default('');
			$table->string('age_range', 8)->default('');
			$table->string('visit_rate', 8)->default('');
			$table->string('living', 10)->default('');
			$table->boolean('survey')->default(0);
			$table->engine='MyISAM';
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
