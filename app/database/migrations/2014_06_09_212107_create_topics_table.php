<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('topics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('t1', 10)->default('');
			$table->string('t2', 10)->default('');
			$table->string('t3', 10)->default('');
			$table->string('t4', 10)->default('');
			$table->string('t5', 10)->default('');
			$table->engine='MyISAM';
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('topics');
	}

}
