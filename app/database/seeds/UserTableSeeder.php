<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$user = User::create(array(
		  'firstname' => 'nada',
		  'lastname' => 'fadali',
		  'email' => 'nada.fadali@gmail.com',
		  'password' => 'cute'
		));

		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			User::create([
				'firstname' => $faker->firstname,
				'lastname' => $faker->lastname,
				'email' => $faker->email,
				'password' => $faker->word
			]);
		}
	}

}